import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './_components/page-not-found/page-not-found.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { SignInComponent } from './_components/sign-in/sign-in.component';
import { HomepageComponent } from './_components/homepage/homepage.component';
import { ItemListEditComponent } from './_components/item-list/item-list-edit.component';
import { ItemListViewComponent } from './_components/item-list/item-list-view.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'sign-in/:token', component: SignInComponent },
  { path: 'item/list/edit', component: ItemListEditComponent },
  { path: 'item/list/edit/:id', component: ItemListEditComponent },
  { path: 'item/list/view/:id', component: ItemListViewComponent },
  { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page];
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: [ ]
})
export class AppRoutingModule { }