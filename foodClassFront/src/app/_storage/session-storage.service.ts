/**
 * @author Quentin Roussel
 * @copyright Genarkys
 */
import { Injectable } from "@angular/core";
import { Storage } from "./storage.service";

@Injectable({providedIn: 'root'})

export class SessionStorage extends Storage {
	override get(key: string): string | null {
		return sessionStorage.getItem(key)
	}

	override set(key: string, value: string): void {
		sessionStorage.setItem(key, value)
	}

	override remove(key: string): void {
		sessionStorage.removeItem(key)
	}

	override clear(): void {
		sessionStorage.clear()
	}
}