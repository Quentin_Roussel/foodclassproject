/**
 * @author Quentin Roussel
 * @copyright Genarkys
 */

export abstract class Storage {
  abstract get(key: string): void 

  abstract set(key: string, value: string): void

  abstract clear(): void

  abstract remove(key: string): void
}