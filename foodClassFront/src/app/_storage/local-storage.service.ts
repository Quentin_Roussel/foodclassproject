/**
 * @author Quentin Roussel
 * @copyright Genarkys
 */
import { Injectable } from "@angular/core";
import { Storage } from "./storage.service";

@Injectable({providedIn: 'root'})

export class LocalStorage extends Storage {
	get(key: string): any | null {
		return localStorage.getItem(key)
	}

	set(key: string, value: string): void {
		localStorage.setItem(key, value)
	}

	remove(key: string): void {
		localStorage.removeItem(key)
	}

	clear(): void {
		localStorage.clear()
	}
}