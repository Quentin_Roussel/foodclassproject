/**
 * @author Quentin Roussel
 * @copyright Genarkys
 */
import { Injectable } from "@angular/core";
import { Storage } from "./storage.service";

@Injectable({providedIn: 'root'})

export class CookieStorage extends Storage {

	override get(key: string): string | null {
    let name: string = key + "="
    let decodedCookie: string = decodeURIComponent(document.cookie)
    let ca: string[] = decodedCookie.split(';')

    for(let i = 0; i < ca.length; i++) {
      let c: string = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }

    return null
	}

	override set(key: string, value: string, expireInDays?: number): void {
    const d = new Date()
    d.setTime(d.getTime() + ((expireInDays ? expireInDays : 1) * 24 * 60 * 60 * 1000))

    let expires = "expires="+d.toUTCString()

    document.cookie = key + "=" + value + ";" + expires + ";path=/"
	}

	override remove(key: string): void {
    return this.set(key, '', 0)
	}

	override clear(): void {
    let decodedCookie: string = decodeURIComponent(document.cookie)
    let ca: string[] = decodedCookie.split(';')

    for(let i = 0; i < ca.length; i++) {
      let c: string = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }

      let t: string[] = c.split('=')
      let name: string = t[0]

      this.set(name, '', 0)
    }
	}
}