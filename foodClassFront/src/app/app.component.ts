import { Component, OnInit } from '@angular/core';
import { UserService } from './_services/user/user.service';
import { User } from './_services/user/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user: User | null = null

  constructor() {}
}
