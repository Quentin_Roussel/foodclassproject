export class HttpFlags {
    id?: string | number
    ids?: string[] | number[]
    direction?: string
    limit?: number
    search?: string
    offset?: string | number
    orderBy?: string
    onlyCountMode?: boolean
    groups?: string
    filterBySingleId?: string
}

export function flagsToString(flags: HttpFlags, separator: string = '='): string {
    let s: string = ''

    Object.keys(flags).forEach(k => {
    })
    
    let x = 0;

    for (let key in flags) {
        s += String(key) + String(separator) + Reflect.get(flags, key)
        if (x + 1 < Object.keys(flags).length) {
            s += '&'
        }
        x++
    }

    return s
}