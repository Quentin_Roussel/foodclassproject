import { Item } from "../item/item";

export class User extends Item {
  firstName: string
  name: string
  email: string
  token: string
}