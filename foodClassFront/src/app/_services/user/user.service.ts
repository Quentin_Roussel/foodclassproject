import { Injectable } from "@angular/core";
import { LocalStorage } from "../../_storage/local-storage.service";
import { User } from "./user";
import { ItemService } from "../item/item.service";

@Injectable({providedIn: 'root'})

export class UserService {
  FV_KEY = '_GEN__FV_KEY'
  USER_SESSION_KEY = '_GEN__US_KEY'
  USER_OBJECT_KEY = '_GEN__US_OBJ_KEY'

  constructor(private lStorageService: LocalStorage, private itemService: ItemService) {}

  public isFirstVisit(): boolean {
    return this.lStorageService.get(this.FV_KEY) !== null
  }

  public userIsSet(): boolean {
    return this.lStorageService.get(this.USER_SESSION_KEY) !== null
  }

  public loadUser(): Promise<User|undefined> {
    return new Promise<User|undefined>((res, rej) => {
      if (this.userIsSet()) {
        res(JSON.parse(this.lStorageService.get(this.USER_OBJECT_KEY)) as User)

        return
      }
      res(undefined) 
      return
    })
  }

  public saveUser(data: any): Promise<boolean> {
    return new Promise<boolean>((res, rej) => {
      this.lStorageService.set(this.USER_SESSION_KEY, this.FV_KEY)
      this.lStorageService.set(this.USER_OBJECT_KEY, JSON.stringify(data))

      res(true)
    })
  }

  public settings(): Promise<any|undefined> {
    return new Promise((res, rej) => {
      if (!this.userIsSet()) {
        rej(undefined)
      }
  
      this.loadUser().then((user) => {
        res(JSON.parse(user?.settings))
      })
    })
  }
}