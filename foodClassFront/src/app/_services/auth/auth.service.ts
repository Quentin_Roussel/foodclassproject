import { Injectable } from "@angular/core";
import * as globalVars from "../../../global"
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { HttpFlags, flagsToString } from "../http-flags";

@Injectable({providedIn: 'root'})


export class AuthService {

  constructor(private http: HttpClient) {}

  public authenticate(data: any): Observable<any> {
    let uri: string = globalVars.API_URI + 'login'

    return this.http.post(uri, data, globalVars.httpOptions)
  }

  public signIn(data: any): Observable<any> {
    let uri: string = globalVars.API_URI + 'sign-in'

    return this.http.post(uri, data, globalVars.httpOptions)
  }

  public validateSignInToken(token: string): Observable<any> {
    let uri: string = globalVars.API_URI + 'sign-in/token/' + token

    return this.http.get(uri)

  }

  public editPassword(data: any): Observable<any> {
    let uri: string = globalVars.API_URI + 'sign-in/password'

    return this.http.post(uri, data, globalVars.httpOptions)
  }
}