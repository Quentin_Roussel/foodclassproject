import { User } from "../user/user"
import { Item } from "./item"

export class SharedItem {
    id: number
    user: User
    item: Item
}