import { Item } from "./item"

export class ItemList {
    id: number
    label: string
    items: Item[] = []
}