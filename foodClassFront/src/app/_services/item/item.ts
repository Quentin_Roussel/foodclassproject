import { SharedItem } from "./shared-item"

export abstract class Item {
  id: number
  createdAt: Date
  updatedAt: Date
  deletedAt: Date
  createdBy: Item | null
  relationIdentifier: number
  relationClassname: string
  isActive: boolean
  [key: string]: any
  settings: any
  // relation to current workspace
  sharedItems: SharedItem[] = []
  targetLink: string
  idealPrice: number
}

export class EntityActions {
  actions: string[] = []
}

export class DisplayedCol {
  type: string
  key: string
  position?: number
  isVisible?: boolean
  hasAlert?: boolean
  relationLabel?: string
  relationIdentifier?: string
  [key: string]: any
}

export function allowedActionsByEntity(entity: string): EntityActions {
  let settings: EntityActions = new EntityActions

  switch (entity) {
    case 'user':
      settings.actions = ['add', 'edit', 'view', 'archive']
      break
    case 'customer':
      settings.actions = ['add', 'edit', 'view', 'archive']
      break
    case 'product':
      settings.actions = ['add', 'edit', 'view', 'archive']
      break
    case 'structure':
      settings.actions = ['add', 'edit', 'view', 'archive']
      break
    default:
      settings.actions = ['add', 'edit', 'view', 'archive']
      break
  }

  return settings
}

export function allowedColsByEntity(entity: string): DisplayedCol[] {
  let cols: DisplayedCol[] = []

  switch (entity) {
    case 'user':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'name'}, 
      {type: 'string', key: 'firstName'}, {type: 'date', key: 'birthAt'}]
    break
    case 'interlocutor':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'name'}, 
      {type: 'string', key: 'firstName'}]
    break
    case 'customer':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'corporateName'}]
    break
    case 'product':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'internalName'}]
    break
    case 'structure':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'internalName'}]
    break
    case 'address':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'label'}]
    break
    case 'country':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'label'}]
    break
    case 'region':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'label'}, {type: 'relation', key: 'country', relationIdentifier: 'id', relationLabel: 'label'}]
    break
    case 'widget':
      cols = [{type: 'string', key: 'id'}, {type: 'string', key: 'label'}]
    break
    default:
    break
  }

  return cols
}

export function getCols(): string[] {
  return ['id']
}