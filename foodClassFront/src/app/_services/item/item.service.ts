import { Injectable } from "@angular/core";
import { Item } from "./item";
import { User } from "../user/user";
import * as globalVars from "../../../global"
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { HttpFlags, flagsToString } from "../http-flags";

@Injectable({providedIn: 'root'})

export class ItemService {

  constructor(private http: HttpClient) {}

  public getItemsAction(entity: string, httpFlags?: HttpFlags): Observable<any> {
    let uri: string = globalVars.API_URI + entity + '/items'

    if (httpFlags) {
      uri += '?' + flagsToString(httpFlags)
    }

    return this.http.get(uri)
  }

  public getItemByQueryAction(entity: string, query: string, value: string | number, 
  httpFlags?: any): Observable<any> {
    let uri: string = globalVars.API_URI + entity + '/item/' + query + '/' + value

    if (httpFlags) {
      uri += '?' + flagsToString(httpFlags)
    }

    return this.http.get(uri)
  }

  public postItemAction(entity: string, data: any, id?: string | number): Observable<any> {
    let uri: string = globalVars.API_URI + entity + '/item/edit'

    if (id) {
      uri += '/' + id
    }

    return this.http.post(uri, data, globalVars.httpOptions)
  }

  public postItemToShare(entity: string, data: any): Observable<any> {
    let uri: string = globalVars.API_URI + entity + '/item/to-share'

    return this.http.post(uri, data, globalVars.httpOptions)
  }

  public postItemToCustomAction(entity: string, data: any, what: string): Observable<any> {
    let uri: string = globalVars.API_URI + entity + '/item/to/' + what

    return this.http.post(uri, data, globalVars.httpOptions)
  }

  public deleteItemAction(entity: string, id: number | string): Observable<any> {
    let uri: string = globalVars.API_URI + entity + '/item/' + id

    return this.http.delete(uri, globalVars.httpOptions)
  }
}