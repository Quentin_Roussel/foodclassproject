import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from './angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './_components/layout/layout.component';
import { HeaderComponent } from './_components/layout/header/header.component';
import { BodyComponent } from './_components/layout/body/body.component';
import { FooterComponent } from './_components/layout/footer/footer.component';
import { LoginComponent } from './_components/login/login.component';
import { FormValidationComponent } from './_components/_form/_validation.component';
import { SignInComponent } from './_components/sign-in/sign-in.component';
import { HomepageComponent } from './_components/homepage/homepage.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { ItemListEditComponent } from './_components/item-list/item-list-edit.component';
import { authInterceptorProviders } from './_auth/auth.interceptor';
import { ItemListCardListComponent } from './_components/item-list/item-list-card-list.component';
import { ItemListViewComponent } from './_components/item-list/item-list-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    LoginComponent,
    FormValidationComponent,
    SignInComponent,
    HomepageComponent,
    DashboardComponent,
    ItemListEditComponent,
    ItemListCardListComponent,
    ItemListViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
    }),
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    authInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
