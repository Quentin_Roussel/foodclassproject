import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/_services/user/user';
import { UserService } from 'src/app/_services/user/user.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
    user: User | null = null
  
    isFirstVisit: boolean = true
    isLogin: boolean = true
  
    @Output() logged: EventEmitter<User> = new EventEmitter()
    constructor(private userService: UserService) {}
  
    ngOnInit(): void {
      this.isFirstVisit = this.userService.isFirstVisit()
      this.setup()
    }
  
    setup(): void {
      this.userService.loadUser().then((u) => {
        if (u) {
          this.user = u
  
          let settings: any = this.user.settings
  
          if (settings) {
            settings = JSON.parse(settings)
  
            if (settings.theming && settings.theming.theme) {
              document.body.classList.remove('theming-white')
              document.body.classList.add('theming-' + settings.theming.theme)
              document.documentElement.lang = 'fr'
            }
          } else {
            document.body.classList.add('theming-white')
          }
  
          return
        }
  
        this.user = null
      })
    }
}
