import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UserService } from '../../_services/user/user.service';
import { User } from '../../_services/user/user';
import { ItemService } from '../../_services/item/item.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ItemList } from 'src/app/_services/item/itemList';
import { HttpFlags } from 'src/app/_services/http-flags';

@Component({
  selector: 'app-item-list-card-list',
  templateUrl: './item-list-card-list.component.html',
  styleUrls: ['./item-list-card-list.component.css']
})
export class ItemListCardListComponent implements OnInit {
  @Input() user: User | null

  items: ItemList[] = []
  sharedItem: ItemList[] = []

  constructor(private itemService: ItemService, private userService: UserService) {}

  ngOnInit(): void {
    let f: HttpFlags = new HttpFlags
    f.filterBySingleId = 'author:' + this.user.id
    this.itemService.getItemsAction('itemList', f).subscribe({
        next: (v) => {
            this.items = v.items
        },
        error: (err) => {

        }
    })

    this.userService.loadUser().then((u) => {
        if (u) {

      let f: HttpFlags = new HttpFlags()
      f.filterBySingleId = 'user:' + u.id
      f.groups = 'item:view_list'
            this.itemService.getItemsAction('sharedItem', f).subscribe({
                next: (value) => {
                    value.items?.forEach(v => {
                        let ae = this.sharedItem.find(si => {
                            return si.id === v.item.itemList.id
                        })

                        if (!ae) {
                            this.sharedItem.push(v.item.itemList)
                            ae = v.item.itemList
                            ae.items = []
                        }

                        ae.items.push(v.item)
                    })
                },
            })
        }
    })
  }

  deleteItemList(itemList: ItemList): void {
    this.itemService.deleteItemAction('itemList', itemList.id).subscribe({
        next: (v) => {
            this.items = this.items.filter(i => {
                return i.id !== itemList.id
            })
        }
    })
  }
}
