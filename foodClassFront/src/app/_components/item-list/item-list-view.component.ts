import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../_services/user/user.service';
import { User } from '../../_services/user/user';
import { ItemService } from '../../_services/item/item.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ItemList } from 'src/app/_services/item/itemList';
import { Observable, ReplaySubject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpFlags } from 'src/app/_services/http-flags';
import { Item } from 'src/app/_services/item/item';

@Component({
  selector: 'app-item-list-view',
  templateUrl: './item-list-view.component.html',
  styleUrls: ['./item-list-view.component.css']
})
export class ItemListViewComponent implements OnInit {
  @Input() user: User | null
  @Input() itemList: ItemList

  form!: FormGroup
  formIsSubmitted: boolean = false

  itemsToShare: any[] = []
  itemSharedToUser: {user: User, items: Item[]}[] = []

  @Input() itemsBank: any[] = []
  @Input() isSearching: boolean = false
  @Input() itemServerSideFilteringCtrl: FormControl = new FormControl('');
  @Input() filteredServerSideItems: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  ctrl: FormControl = new FormControl('')
  filteredOptions: Observable<any[]>;

  constructor(private userService: UserService, private itemService: ItemService,
    private fb: FormBuilder, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.userService.loadUser().then((user) => {
      if (user) {
        this.user = user
      }
    })

    let flags: HttpFlags = new HttpFlags()
    flags.groups = 'item:shared'

    this.itemService.getItemsAction('user', flags).subscribe(
        data => {
          this.itemsBank = data.items
          this.filteredOptions = this.ctrl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value || '')),
          );
        }
      )

    this.route.params.subscribe((params) => {
        if (params.id) {
            this.itemService.getItemByQueryAction('itemList', 'id', params.id).subscribe({
                next: (v) => {
                    this.itemList = v
                }
            })
        }
    })
  }

  submitShare(): void {
    let data = {
      user: this.ctrl?.value?.id,
      itemToShare: this.itemsToShare
    }

    this.formIsSubmitted = true

    this.itemService.postItemToShare('itemList', data).subscribe({
        next: (v) => {
          this.formIsSubmitted = false
          this.itemsToShare = []
          this.ctrl.setValue('')
        },
    })
  }

  removeSelected(): void {
    this.formIsSubmitted = true
    this.itemsToShare.forEach(its => {
      this.itemService.deleteItemAction('item', its).subscribe({
        next: (v) => {
          this.itemList.items = this.itemList.items.filter(i => {
            return i.id !== its
          })
          this.itemsToShare = this.itemsToShare.filter(itsId => { return its !== itsId })
          if (this.itemsToShare.length <= 0) {
            this.formIsSubmitted = false
          }
        }
      })
    })
  }

  unshareSelected(userId: number): void {
    this.formIsSubmitted = true

    let ae = this.itemSharedToUser.find(istu => {
      return istu.user.id === userId
    })

    if (ae) {
      let data = {
        user: userId,
        itemIds: this.itemsToShare
      }
  
      this.itemService.postItemToCustomAction('sharedItem', data, 'unshare').subscribe({
        next: (v) => {
          this.formIsSubmitted = false
          this.itemSharedToUser = this.itemSharedToUser.filter(istu => {
            return istu.user.id !== userId
          })

          this.itemList.items.forEach(i => {
            i.sharedItems = i.sharedItems.filter(si => {
              return si.user.id !== userId
            })
          })
        }
      })
    }
  }

  toggleItem(item: any): void {
    if (this.itemsToShare.includes(item.id)) {
        this.itemsToShare = this.itemsToShare.filter(its => { return its !== item.id })
        return
    }

    this.itemsToShare.push(item.id)

    item.sharedItems.forEach(si => {
      let ae = this.itemSharedToUser.find(istu => {
        return istu.user.id === si.user.id
      })

      if (!ae) {
        ae = {user: si.user, items: [] = []}
        this.itemSharedToUser.push(ae)
      }

      let iae = ae.items.find(it => {
        return it.id === item.id
      })

      if (!iae) {
        ae.items.push(item)
      }
    })
  }

  private _filter(search: any): any[] {
    if (String(search).length <= 0) {
      return this.itemsBank
    }

    let filterValue: string = ''

    if (typeof search === 'string') {
      filterValue = search.toLocaleLowerCase()
    } else {
      filterValue = search.email
    }

    return this.itemsBank.filter(option => option.email.toLowerCase().includes(filterValue));
  }

  displayFn(item: any): string {
    return item && item.email ? item.email : ''
  }
}
