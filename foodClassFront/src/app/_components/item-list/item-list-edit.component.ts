import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UserService } from '../../_services/user/user.service';
import { User } from '../../_services/user/user';
import { ItemService } from '../../_services/item/item.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ItemList } from 'src/app/_services/item/itemList';
import { Item } from 'src/app/_services/item/item';

@Component({
  selector: 'app-item-list-edit',
  templateUrl: './item-list-edit.component.html',
  styleUrls: ['./item-list-edit.component.css']
})
export class ItemListEditComponent implements OnInit, OnChanges {
  @Input() user: User | null
  @Input() itemList: ItemList

  form!: FormGroup
  formIsSubmitted: boolean = false

  constructor(private userService: UserService, private itemService: ItemService,
    private fb: FormBuilder, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.userService.loadUser().then((user) => {
      if (user) {
        this.user = user
      }
    })

    this.route.params.subscribe((params) => {
      if (params.id) {
        this.itemService.getItemByQueryAction('itemList', 'id', params.id).subscribe({
          next: (v) => {
            this.itemList = v
            this.makeForm()
          }
        })
      }
    })

    this.makeForm()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['user']) {
      this.user = changes['user'].currentValue
    }
  }

  makeForm(): void {
    this.form = new FormGroup({
      label: new FormControl(this.itemList?.label, [Validators.required]),
      items: this.fb.array([])
    })

    this.itemList?.items?.forEach(i => {
      this.addItem(i)
    })
  }

  addItem(item?: Item): void {
    let fg: FormGroup = new FormGroup({
      label: new FormControl(item?.label, [Validators.required]),
      targetLink: new FormControl(item?.targetLink),
      idealPrice: new FormControl(item?.idealPrice),
    })

    this.getFormArray('items').push(fg)
  }

  submitForm(): void {
    this.formIsSubmitted = true
    this.form.markAllAsTouched()

    if (this.form.invalid) {
      this.formIsSubmitted = false
      console.warn('invalid')
      return
    }

    let data = this.form.value

    this.itemService.postItemAction('itemList', data, this.itemList?.id).subscribe({
      next: (v) => {
        this.formIsSubmitted = false
      },
      error: (e) => {
        this.formIsSubmitted = false
      }
    })
  }

  getFormControl(key: string): FormControl {
    return this.form.get(key) as FormControl
  }

  getFormArray(key: string): FormArray {
    return this.form.get(key) as FormArray
  }

  getFormArrayGroup(arrKey: string, index: number): FormGroup {
    return this.getFormArray(arrKey).at(index) as FormGroup
  }

  getFormArrayGroupControl(arrKey: string, index: number, controlName: string): FormControl {
    return this.getFormArrayGroup(arrKey, index).get(controlName) as FormControl
  }

  getFormGroup(id: string): FormGroup {
    return this.form.get(id) as FormGroup
  }
}
