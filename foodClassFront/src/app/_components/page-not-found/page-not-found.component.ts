import { Component, Input } from '@angular/core';
import { Item } from '../../_services/item/item';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.sass']
})
export class PageNotFoundComponent {
  @Input() user: Item | null
}
