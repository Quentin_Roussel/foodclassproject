import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../_services/auth/auth.service';
import { UserService } from '../../_services/user/user.service';
import { ItemService } from '../../_services/item/item.service';
import { Router } from '@angular/router';
import { LocalStorage } from '../../_storage/local-storage.service';
import { User } from '../../_services/user/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  form!: FormGroup
  formIsSubmitted: boolean = false
  customError: string = ''
  passwordIsShow: boolean = false

  @Output() logged: EventEmitter<User> = new EventEmitter()
  @Output() wouldSignIn: EventEmitter<boolean> = new EventEmitter()
  constructor(private authService: AuthService, private userService: UserService,
    private router: Router, private lStorageService: LocalStorage) {}

  ngOnInit(): void {
    this.makeForm()
  }

  makeForm(): void {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
  }

  submitForm(): void {
    this.formIsSubmitted = true
    this.customError = ''
    this.form.markAllAsTouched()

    if (this.form.invalid) {
      this.formIsSubmitted = false
      return
    }

    let data = this.form.value

    this.authService.authenticate(data).subscribe({
      next: (v) => {
        this.lStorageService.set('TOKEN', v.token)
        this.formIsSubmitted = false
        this.userService.saveUser(v).then(() => {
          window.location.href = '/'
        })
      },
      error: (e) => {
        this.formIsSubmitted = false
        this.customError = e.error.message
      }
    })
  }

  wouldSign(): void {
    this.wouldSignIn.emit(true)
  }

  get username() { return this.form.get('username') as FormControl }
  get password() { return this.form.get('password') as FormControl }
}
