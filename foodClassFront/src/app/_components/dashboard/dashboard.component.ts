import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UserService } from '../../_services/user/user.service';
import { User } from '../../_services/user/user';
import { ItemService } from '../../_services/item/item.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit, OnChanges {
  @Input() user: User | null

  constructor(private userService: UserService, private itemService: ItemService) {}

  ngOnInit(): void {
    this.userService.loadUser().then((user) => {
      if (user) {
        this.user = user
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['user']) {
      this.user = changes['user'].currentValue
    }
  }
}
