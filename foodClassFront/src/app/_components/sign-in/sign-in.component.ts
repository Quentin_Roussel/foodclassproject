import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../_services/auth/auth.service';
import { UserService } from '../../_services/user/user.service';
import { ItemService } from '../../_services/item/item.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '../../_storage/local-storage.service';
import { User } from '../../_services/user/user';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  form!: FormGroup
  formIsSubmitted: boolean = false
  customError: string = ''
  passwordIsShow: boolean = false

  hasSignIn: boolean = false

  hasToken: boolean = false
  tokenIsVerified: boolean = false
  tokenVerificationInProgress: boolean = false

  token: string = ''

  seePlainPassword: boolean = false

  @Output() logged: EventEmitter<User> = new EventEmitter()
  @Output() cancelSignIn: EventEmitter<boolean> = new EventEmitter()
  constructor(private authService: AuthService, private userService: UserService, private route: ActivatedRoute,
    private itemService: ItemService, private router: Router, private lStorageService: LocalStorage) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params['token']) {
        this.token = params['token']
        this.hasToken = true
        this.tokenVerification().then((v) => {
          this.tokenIsVerified = true
          this.tokenVerificationInProgress = false
          this.makePasswordForm()
        })
      }
    })
    this.makeForm()
  }

  makeForm(): void {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email])
    })
  }

  submitForm(): void {
    this.formIsSubmitted = true
    this.customError = ''
    this.form.markAllAsTouched()

    if (this.form.invalid) {
      this.formIsSubmitted = false
      return
    }

    let data = this.form.value

    this.authService.signIn(data).subscribe({
      next: (v) => {
        this.formIsSubmitted = false
        if (v.state === true) {
          this.hasSignIn = true
          return
        }

        this.customError = v.error ?? 'Une erreur inconnue a été détéctée...'
      },
      error: (e) => {
        this.formIsSubmitted = false
      }
    })
  }

  wouldCancelSignIn(): void {
    this.cancelSignIn.emit(true)
  }

  tokenVerification(): Promise<any> {
    return new Promise((res, rej) => {
        this.tokenVerificationInProgress = true
        this.authService.validateSignInToken(this.token).subscribe({
            next: (v) => {
                res(v)
            },
            error: (err) => {
                rej(err)
            }
        })
    })
  }

  makePasswordForm(): void {
    this.form = new FormGroup({
        newPassword: new FormControl('', [Validators.required]),
        confirmPassword: new FormControl('', [Validators.required])
    })
  }

  submitPasswordForm(): void {
    this.formIsSubmitted = true
    this.form.markAllAsTouched()

    if (this.form.invalid) {
      this.formIsSubmitted = false
      return
    }

    let data = this.form.value

    data.token = this.token

    this.authService.editPassword(data).subscribe({
      next: (v) => {
        this.hasSignIn = true
        window.location.href = '/'
      },
      error: (e) => {
        this.formIsSubmitted = false
      }
    })
  }

  getFormControl(key: string): FormControl {
    return this.form.get(key) as FormControl
  }
}
