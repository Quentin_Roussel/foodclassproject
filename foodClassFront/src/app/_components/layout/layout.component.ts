import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../_services/user/user';
import { UserService } from 'src/app/_services/user/user.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent implements OnInit {
  @Input() user: User | null

  @Output() userReload: EventEmitter<any> = new EventEmitter()
  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.loadUser().then((u) => {
      if (u) {
        this.user = u
      }
    })
  }
}
