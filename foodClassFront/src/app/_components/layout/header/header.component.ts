import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { User } from '../../../_services/user/user';
import { FormControl } from '@angular/forms';
import { LocalStorage } from '../../../_storage/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnChanges {
  @Input() user: User | null
  userSettings: any

  isSearching: boolean = false
  searchCtrl: FormControl = new FormControl()

  @Output() userReload: EventEmitter<any> = new EventEmitter()
  constructor(private lStorageService: LocalStorage, private router: Router) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['user']) {
      this.user = changes['user'].currentValue
    }
  }

  toggleSearch(isSearching: boolean): void {
    this.isSearching = isSearching
  }

  logout(): void {
    this.lStorageService.clear()
    window.location.href = '/'
  }
}
