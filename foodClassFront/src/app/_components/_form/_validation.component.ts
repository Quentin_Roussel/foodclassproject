import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item } from '../../_services/item/item';
import { UserService } from '../../_services/user/user.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-validation',
  templateUrl: './_validation.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormValidationComponent implements OnInit {
  @Input() user: Item | null
  @Input() form!: FormGroup
  @Input() formIsSubmitted: boolean = false
  @Input() formSettings: any

  @Output() submit: EventEmitter<any> = new EventEmitter()

  constructor(private userService: UserService) {}

  ngOnInit(): void {
  }
}
