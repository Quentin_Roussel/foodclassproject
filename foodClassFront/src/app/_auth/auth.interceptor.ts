import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LocalStorage } from '../_storage/local-storage.service';

const TOKEN_HEADER_KEY = 'Authorization';       // for Spring Boot back-end

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private lsService: LocalStorage, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    let httpReq: string = req.url.substring(0, 5)
    if (httpReq === 'https') { return next.handle(req) }
    const token = this.lsService.get('TOKEN')
    let bearer = 'Bearer ' + token
    if (token != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, bearer) });
    }
    return next.handle(authReq).pipe(
      catchError((error) => {
        if (error.status && error.status == '403' || error.statusText && error.statusText == 'Forbidden') {
          // @logic needed
        }

        return throwError(error)
      })
    );
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];