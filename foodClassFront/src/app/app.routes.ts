import { Routes } from '@angular/router';

export const routes: Routes = [
  // { path: '', component: AppComponent },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
];
