import { HttpHeaders } from '@angular/common/http';
import { environment } from './environments/environment';
'use strict';

export const API_URI = 'http://127.0.0.1:8000/api/'
export const TD_ACTIONS_KEY = 'actions'
export const httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  
if (environment.production) {

}