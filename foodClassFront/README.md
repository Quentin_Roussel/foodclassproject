# FoodClass Front

## Pré-requis
[Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

NodeJS version 18.19.x.

## Installation

1 - Depuis le répertoire foodClassFront/ lancer la commande : **npm install**.

2 - Depuis le répertoire foodClassApi/ lancer la commande : **export NODE_OPTIONS=--openssl-legacy-provider && ng serve**.