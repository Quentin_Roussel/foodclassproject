<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/annotations' => array(
            'pretty_version' => '1.14.3',
            'version' => '1.14.3.0',
            'reference' => 'fb0d71a7393298a7b232cbf4c8b1f73f3ec3d5af',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/annotations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/cache' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'reference' => '1ca8f21980e770095a31456042471a57bc4c68fb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/collections' => array(
            'pretty_version' => '2.1.4',
            'version' => '2.1.4.0',
            'reference' => '72328a11443a0de79967104ad36ba7b30bded134',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/collections',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/common' => array(
            'pretty_version' => '3.4.3',
            'version' => '3.4.3.0',
            'reference' => '8b5e5650391f851ed58910b3e3d48a71062eeced',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/common',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/dbal' => array(
            'pretty_version' => '3.8.1',
            'version' => '3.8.1.0',
            'reference' => 'c9ea252cdce4da324ede3d6c5913dd89f769afd2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/dbal',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => '1.1.3',
            'version' => '1.1.3.0',
            'reference' => 'dfbaa3c2d2e9a9df1118213f3b8b0c597bb99fab',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/doctrine-bundle' => array(
            'pretty_version' => '2.11.3',
            'version' => '2.11.3.0',
            'reference' => '492725310ae9a1b5b20d6ae09fb5ae6404616e68',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../doctrine/doctrine-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/doctrine-migrations-bundle' => array(
            'pretty_version' => '3.3.0',
            'version' => '3.3.0.0',
            'reference' => '1dd42906a5fb9c5960723e2ebb45c68006493835',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../doctrine/doctrine-migrations-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/event-manager' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'reference' => '95aa4cb529f1e96576f3fda9f5705ada4056a520',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/event-manager',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '2.0.9',
            'version' => '2.0.9.0',
            'reference' => '2930cd5ef353871c821d5c43ed030d39ac8cfe65',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'c6222283fa3f4ac679f8b9ced9a4e23f163e80d0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '2.1.1',
            'version' => '2.1.1.0',
            'reference' => '861c870e8b75f7c8f69c146c7f89cc1c0f1b49b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/migrations' => array(
            'pretty_version' => '3.7.2',
            'version' => '3.7.2.0',
            'reference' => '47af29eef49f29ebee545947e8b2a4b3be318c8a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/migrations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/orm' => array(
            'pretty_version' => '2.18.0',
            'version' => '2.18.0.0',
            'reference' => 'f2176a9ce56cafdfd1624d54bfdb076819083d5b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/persistence' => array(
            'pretty_version' => '3.2.0',
            'version' => '3.2.0.0',
            'reference' => '63fee8c33bef740db6730eb2a750cd3da6495603',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/persistence',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/sql-formatter' => array(
            'pretty_version' => '1.1.3',
            'version' => '1.1.3.0',
            'reference' => '25a06c7bf4c6b8218f47928654252863ffc890a5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/sql-formatter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'egulias/email-validator' => array(
            'pretty_version' => '4.0.2',
            'version' => '4.0.2.0',
            'reference' => 'ebaaf5be6c0286928352e054f2d5125608e5405e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../egulias/email-validator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'friendsofsymfony/rest-bundle' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'reference' => 'cb75a41227ed5a9028fa3028c8225dda5c7b3086',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../friendsofsymfony/rest-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'handcraftedinthealps/rest-routing-bundle' => array(
            'pretty_version' => '1.0.6',
            'version' => '1.0.6.0',
            'reference' => 'dc3dd8ef675da5bad22c6498c888c52c2f4289a6',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../handcraftedinthealps/rest-routing-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jms/metadata' => array(
            'pretty_version' => '2.8.0',
            'version' => '2.8.0.0',
            'reference' => '7ca240dcac0c655eb15933ee55736ccd2ea0d7a6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jms/metadata',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jms/serializer' => array(
            'pretty_version' => '3.29.1',
            'version' => '3.29.1.0',
            'reference' => '111451f43abb448ce297361a8ab96a9591e848cd',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jms/serializer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jms/serializer-bundle' => array(
            'pretty_version' => '5.4.0',
            'version' => '5.4.0.0',
            'reference' => '6fa2dd0083e00fe21c5da171556d7ecabc14b437',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../jms/serializer-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nikic/php-parser' => array(
            'pretty_version' => 'v4.18.0',
            'version' => '4.18.0.0',
            'reference' => '1bcbb2179f97633e98bbbc87044ee2611c7d7999',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/php-parser',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpstan/phpdoc-parser' => array(
            'pretty_version' => '1.25.0',
            'version' => '1.25.0.0',
            'reference' => 'bd84b629c8de41aa2ae82c067c955e06f1b00240',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpstan/phpdoc-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => '213f9dbc5b9bfbc4f8db86d2838dc968752ce13b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/event-dispatcher' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'ef29f6d262798707a9edd554e2b82517ef3a9376',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'sensio/framework-extra-bundle' => array(
            'pretty_version' => 'v5.6.1',
            'version' => '5.6.1.0',
            'reference' => '430d14c01836b77c28092883d195a43ce413ee32',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../sensio/framework-extra-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'db1adb004e2da984085d0178964eb6f319d3cba1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '64be4a7acb83b6f2bf6de9a02cee6dad41277ebc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/config' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '6b763438a22a4f20885e994ad6702f6a3f25430e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/config',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'dbdf6adcb88d5f83790e1efb57ef4074309d3931',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/dependency-injection' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '45474d527212ca67cdb93f6c5e6da68f4bc67118',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/dependency-injection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.4.0',
            'version' => '3.4.0.0',
            'reference' => '7c3aff79d10325257a001fcf92d991f24fc967cf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/doctrine-bridge' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'f1c8d1f75ede8ba6810498666159c50ca05beec0',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/doctrine-bridge',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/dotenv' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '4de488440104b99d2e7c0717ee432e760b061e32',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/dotenv',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/error-handler' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '39225b1e47fdd91a6924b1e7d7a4523da2e1894b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/error-handler',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '7a69a85c7ea5bdd1e875806a99c51a87d3a74b38',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'v3.4.0',
            'version' => '3.4.0.0',
            'reference' => 'a76aed96a42d2b521153fb382d418e30d18b59df',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.0',
            ),
        ),
        'symfony/filesystem' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '5a553607d4ffbfa9c0ab62facadea296c9db7086',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/filesystem',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'abe6d6f77d9465fed3cd2d029b29d03b56b56435',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/flex' => array(
            'pretty_version' => 'v2.4.4',
            'version' => '2.4.4.0',
            'reference' => 'bec213c39511eda66663baa2ee7440c65f89c695',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/flex',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/form' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'c827d421caccf467ef1a969e7412a8755fa88093',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/form',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/framework-bundle' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '89805687f360133f18bdedfb32138ce0ddd5383c',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../symfony/framework-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/google-mailer' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '6d7191a30fd2ae0ab740a93cfbf7fba64834aed6',
            'type' => 'symfony-mailer-bridge',
            'install_path' => __DIR__ . '/../symfony/google-mailer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'f2ab692a22aef1cd54beb893aa0068bdfb093928',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-kernel' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '949bc7721c83fa9f81fc6c9697db0aa340c64f4d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-kernel',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/mailer' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '664724b0fb4646dee30859d0ed9131a2d7633320',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mailer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/maker-bundle' => array(
            'pretty_version' => 'v1.50.0',
            'version' => '1.50.0.0',
            'reference' => 'a1733f849b999460c308e66f6392fb09b621fa86',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../symfony/maker-bundle',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/mime' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'ee94d9b538f93abbbc1ee4ccff374593117b04a9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v5.4.21',
            'version' => '5.4.21.0',
            'reference' => '4fe5cf6ede71096839f0e4b4444d65dd3a7c1eb9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/password-hasher' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '23b9782de5d06a7e61101558d3e887100fbf8f93',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/password-hasher',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-iconv' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => '32a9da87d7b3245e09ac426c83d334ae9f06f80f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-icu' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => '07094a28851a49107f3ab4f9120ca2975a64b6e1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-icu',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => 'a287ed7475f85bf6f61890146edbc932c0fff919',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => 'bc45c394692b948b4d383a08d7753968bed9a83d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => '9773676c8a1bb1f8d4340a62efe641cf76eda7ec',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => '21bd091060673a1177ae842c0ef8fe30893114d2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => '87b68208d5c1188808dd7839ee1e6c8ec3b02f1b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => 'c565ad1e63f30e7477fc40738343c62b40bc672d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-uuid' => array(
            'pretty_version' => 'v1.29.0',
            'version' => '1.29.0.0',
            'reference' => '3abdd21b0ceaa3000ee950097bc3cf9efc137853',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-uuid',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'cbc28e34015ad50166fc2f9c8962d28d0fe861eb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/property-access' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'f1341758d8046cfff0ac748a0cad238f917191d4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/property-access',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/property-info' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'd30d48f366ad2bfbf521256be85eb1c182c29198',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/property-info',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/routing' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '86c5a06a61ddaf17efa1403542e3d7146af96203',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/routing',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/runtime' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'a32a623d71fc0f699a2a196377b3b85c840bd39a',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/runtime',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/security-bundle' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'ed194715515a87d0f9c80b8696baf37ae18beb81',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../symfony/security-bundle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/security-core' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '3cbacefb2a350ed39950f93c8a054c2eb625fb69',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/security-core',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/security-csrf' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '6728ed79d7f9aae3b86fca7ea554f1c46bae1e0b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/security-csrf',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/security-guard' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'b6fb8c88f7cd544db761de2d1c3618cbc5c1b9e7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/security-guard',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/security-http' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '274a6aef49a0e1707bcb57217251885be749b6d8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/security-http',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/serializer' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '02acd86290077dab2f12ae91b3e9f141c079d84c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/serializer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/stopwatch' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '887762aa99ff16f65dc8b48aafead415f942d407',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/stopwatch',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'c209c4d0559acce1c9a2067612cfb5d35756edc2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '136b19dd05cdf0709db6537d058bcab6dd6e2dbe',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/twig-bridge' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'e6ae362b7c4f1d6e99f61f59b0c93b9f027b4c73',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/twig-bridge',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/twig-bundle' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'f59b91e23c7c790e71a187c3fa4aefdc5391d682',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../symfony/twig-bundle',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/uid' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'c280434b875236cd2b1bed1505aecc6141db73e8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/uid',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/validator' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => '4993e9b5b17e23e7ad9dc2e3b31412f5c4679385',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/validator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'ce4685b30e47d94dfc990c5566285ff99ddf012b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/var-exporter' => array(
            'pretty_version' => 'v6.4.3',
            'version' => '6.4.3.0',
            'reference' => 'a8c12b5448a5ac685347f5eeb2abf6a571ec16b8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-exporter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/web-profiler-bundle' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'd71d64739a77ba23e7797d5ed3226796751df058',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../symfony/web-profiler-bundle',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v5.4.35',
            'version' => '5.4.35.0',
            'reference' => 'e78db7f5c70a21f0417a31f414c4a95fe76c07e4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v3.8.0',
            'version' => '3.8.0.0',
            'reference' => '9d15f0ac07f44dc4217883ec6ae02fd555c6f71d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'willdurand/jsonp-callback-validator' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => '1a7d388bb521959e612ef50c5c7b1691b097e909',
            'type' => 'library',
            'install_path' => __DIR__ . '/../willdurand/jsonp-callback-validator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'willdurand/negotiation' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'reference' => '68e9ea0553ef6e2ee8db5c1d98829f111e623ec2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../willdurand/negotiation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
