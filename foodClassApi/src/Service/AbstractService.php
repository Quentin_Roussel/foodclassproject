<?php

namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;

abstract class AbstractService
{
    protected $em;
    protected $mailerInterface;

    public function __construct(EntityManagerInterface $em, MailerInterface $mailerInterface)
    {
        $this->em = $em;
        $this->mailerInterface = $mailerInterface;
    }

    public function getRepository(string $entityName): ServiceEntityRepository
    {
        $className = 'App\\Entity\\' . ucfirst($entityName);

        return $this->em->getRepository($className);
    }
}