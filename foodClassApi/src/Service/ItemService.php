<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\ItemList;
use App\Entity\SharedItem;
use App\Entity\User;
use DateTime;
use DateTimeImmutable;

class ItemService extends AbstractService
{
    /**
     * @param array $flags
     * 
     * @return Item[]
     */
    public function findAllByFlags(string $entityName, array $flags = []): array
    {
        return $this->getRepository($entityName)->findAllByFlags($entityName, $flags);
    }

    /**
     * @param array $flags
     * 
     * @return int
     */
    public function countAllByFlags(string $entityName, array $flags = []): int
    {
        return $this->getRepository($entityName)->countAllByFlags($entityName, $flags);
    }

    /**
     * @param string $entityName
     * @param array $query
     * @param array $value
     * @param array $flags
     */
    public function findByQuery(string $entityName, string $query,
    string $value, array $flags = [])
    {
        return $this->getRepository($entityName)->findOneBy([
            $query => $value
        ]);
    }

    public function edit(ItemList $itemList)
    {
        if (null === $itemList->getCreatedAt()) {
            $itemList->setCreatedAt(new DateTimeImmutable());
        }

        /** @var Item $item */
        foreach ($itemList->getItems() as $item) {
            if (null === $item->getCreatedAt()) {
                $item->setCreatedAt(new DateTimeImmutable());
            }

            $item->setUpdatedAt(new DateTime());
            $item->setIsActive(true);
            $item->setItemList($itemList);
        }

        $itemList->setUpdatedAt(new DateTime());
        $itemList->setIsActive(true);

        $this->em->persist($itemList);
        $this->em->flush();

        return $itemList;
    }

    public function addSharedItemToUser(int $userId, array $itemIds = []): bool
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId]);

        if (null === $user) {
            return false;
        }

        foreach ($itemIds as $itemId) {
            $item = $this->em->getRepository(Item::class)->findOneBy(['id' => $itemId]);

            if (null !== $item) {
                $sharedItem = new SharedItem();
                $sharedItem->setItem($item);
                $sharedItem->setUser($user);

                $this->em->persist($sharedItem);
            }
        }

        $this->em->flush();

        return true;
    }

    public function unsharedItemToUser(int $userId, array $itemIds): void
    {
        foreach ($itemIds as $itemId) {
            $sharedItem = $this->em->getRepository(SharedItem::class)->findOneBy([
                'user' => $userId,
                'item' => $itemId
            ]);

            if ($sharedItem) {
                $this->em->remove($sharedItem);
            }
        }

        $this->em->flush();
    }

    public function deleteSharedItems(array $itemIds): void
    {
        foreach ($itemIds as $itemId) {
            $sharedItem = $this->em->getRepository(SharedItem::class)->findOneByQuery([
                'item' => $itemId
            ]);

            if ($sharedItem) {
                $this->em->remove($sharedItem);
            }
        }

        $this->em->flush();
    }

    public function deleteItem(Item $item): void
    {
        foreach ($item->getSharedItems() as $sharedItem) {
            $this->em->remove($sharedItem);
        }

        $this->em->remove($item);
        $this->em->flush();
    }

    public function deleteItemList(ItemList $itemList): void
    {
        $this->em->remove($itemList);
        $this->em->flush();
    }
}