<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use DateTimeImmutable;

class UserService extends AbstractService
{
    public $currentErrorString;

    public function signIn(string $email): bool
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getRepository('user');

        $alreadyExist = $userRepository->findOneBy(['email' => $email]);

        if (null !== $alreadyExist) {
            $this->currentErrorString = 'Cette adresse email semble déjà associée à un compte.';
            return false;
        }

        $token = sha1(time());
        $signInExpireAt = new DateTime();
        $signInExpireAt->add(new DateInterval('P2D'));

        $newUser = new User();
        $newUser->setEmail($email);
        $newUser->setSignInToken($token);
        $newUser->setSignInExpireAt(new DateTimeImmutable($signInExpireAt->format('Y-m-d H:i:s')));

        $this->edit($newUser);

        $mailerService = new MailerService($this->em, $this->mailerInterface);

        $txt = '<html><body><div><p><a href="localhost:4200/sign-in/"' . $token . '">Finaliser mon inscription</a><br />
        Si le lien ne fonctionne pas, veuillez utiliser ce lien => http://localhost:4200/sign-in/' . $token . '</p></div></body></html>';

        $mailerService->send('quentin.roussel@genarkys.fr', $email, 'Inscription à ShareList !', $txt, $txt);

        return true;
    }

    public function validateToken(?string $token = null): bool
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getRepository('user');

        /** @var User $alreadyExist */
        $alreadyExist = $userRepository->findOneBy(['signInToken' => $token]);

        if (null === $alreadyExist) {
            return false;
        }

        $today = new DateTimeImmutable();

        return $today < $alreadyExist->getSignInExpireAt();
    }

    /**
     * @param User user object to edit
     * @return User user object updated
     */
    public function edit(User $item): User
    {
        if (null === $item->getCreatedAt()) {
            $item->setCreatedAt(new DateTimeImmutable());
        }

        if (null === $item->getIsActive()) {
            $item->setIsActive(false);
        }

        $item->setUpdatedAt(new DateTime());

        $this->em->persist($item);
        $this->em->flush();

        return $item;
    }
}