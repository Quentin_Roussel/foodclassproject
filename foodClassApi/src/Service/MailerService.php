<?php

namespace App\Service;

use Symfony\Component\Mime\Email;

class MailerService extends AbstractService
{
    public function send(string $from, string $to, string $subject, string $text, string $html = null)
    {
        $email = (new Email())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->text($text)
            ->html($html)
        ;

        $this->mailerInterface->send($email);
    }
}