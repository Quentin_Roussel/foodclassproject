<?php

namespace App\Form\Serializer;

use FOS\RestBundle\Serializer\Normalizer\FormErrorNormalizer;
use LogicException as GlobalLogicException;
use Symfony\Component\Form\Form;

class FormErrorSerializer
{
    public function arrayToString(object $form, array $submittedData = []): array
    {
        /** @var Form $form */
        $errors = [];

        $formErrorNormalizer = new FormErrorNormalizer();

        $n = $formErrorNormalizer->normalize($form);

        $formFields = $n['errors'];
        //@Todo tester le cas en datetime avec le tableau
        if (!isset($formFields['children'])) {
            throw new GlobalLogicException('Fatal error');
        }
        foreach ($formFields['children'] as $key => $array) {
            if (!isset($array['errors'])) {
                continue;
            }
            foreach ($array['errors'] as $error) {
                if (preg_match('/value is not valid/', $error)) {
                    $linkedId = isset($submittedData[$key]) ? $submittedData[$key] : '#Aucune valeur#';
                    if (is_array($linkedId)) {
                        $linkedId = $key;
                    }
                    $error = "La liaison <b>$linkedId</b> n'existe pas sur le portail ou cette valeur n'est pas valide.";
                    
                }
                $errors[] = "<b>" . ucfirst($key) . " :</b> " . $error . "";
            }
        }
        if (isset($formFields['errors'])) {
            foreach ($formFields['errors'] as $key => $error) {
                if (preg_match('/not contain extra fields/', $error)) {
                    $error = 'Des valeurs non attendues sont transmises.';
                }
                $errors[] = $error;
            }
        }
        if (empty($errors)) {
            return [];
        }

        $errors = implode('<br />', $errors);

        return [
            'message' => 'Une ou plusieurs erreur(s) détéctée(s)',
            'erreur' => $errors
        ];
    }
}