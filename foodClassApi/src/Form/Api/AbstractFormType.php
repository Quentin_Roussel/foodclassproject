<?php

namespace App\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AbstractFormType extends AbstractType
{
  const DEFAULT_FALSE_VALUES = ['false', '0', 'zero', 'no', false, 0, null];

    public static function getFalseValues(...$vals): array
    {
      $falseValues = [];
      
      foreach($vals as $val) {
        $falseValues[] = $val;
      }

      return $falseValues;
    }

    public static function setupBuilder(FormBuilderInterface $builder, $item)
    {
      $traitName = 'App\\Entity\\EntityIdentity';

      if (true === in_array($traitName, class_uses($item))) {
        $builder
        // EntityIdentity trait
          ->add('label', TextType::class)
          ->add('slug', TextType::class)
        ;
      }

      return $builder;
    }
}