<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use EntityBag;

    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(['item:list', 'item:view', 'item:shared'])]
    private $email;

    /**
     * @var string The hashed password
     */
    #[ORM\Column(nullable: true)]
    private ?string $password = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $signInToken = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $signInExpireAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $apiToken = null;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: ItemList::class)]
    private Collection $itemLists;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: SharedItem::class)]
    private Collection $sharedItems;

    public function __construct()
    {
        $this->itemLists = new ArrayCollection();
        $this->sharedItems = new ArrayCollection();
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->getEmail();
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getSignInToken(): ?string
    {
        return $this->signInToken;
    }

    public function setSignInToken(?string $signInToken): static
    {
        $this->signInToken = $signInToken;

        return $this;
    }

    public function getSignInExpireAt(): ?\DateTimeImmutable
    {
        return $this->signInExpireAt;
    }

    public function setSignInExpireAt(?\DateTimeImmutable $signInExpireAt): static
    {
        $this->signInExpireAt = $signInExpireAt;

        return $this;
    }

    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    public function setApiToken(?string $apiToken): static
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * @return Collection<int, ItemList>
     */
    public function getItemLists(): Collection
    {
        return $this->itemLists;
    }

    public function addItemList(ItemList $itemList): static
    {
        if (!$this->itemLists->contains($itemList)) {
            $this->itemLists->add($itemList);
            $itemList->setAuthor($this);
        }

        return $this;
    }

    public function removeItemList(ItemList $itemList): static
    {
        if ($this->itemLists->removeElement($itemList)) {
            // set the owning side to null (unless already changed)
            if ($itemList->getAuthor() === $this) {
                $itemList->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SharedItem>
     */
    public function getSharedItems(): Collection
    {
        return $this->sharedItems;
    }

    public function addSharedItem(SharedItem $sharedItem): static
    {
        if (!$this->sharedItems->contains($sharedItem)) {
            $this->sharedItems->add($sharedItem);
            $sharedItem->setUser($this);
        }

        return $this;
    }

    public function removeSharedItem(SharedItem $sharedItem): static
    {
        if ($this->sharedItems->removeElement($sharedItem)) {
            // set the owning side to null (unless already changed)
            if ($sharedItem->getUser() === $this) {
                $sharedItem->setUser(null);
            }
        }

        return $this;
    }
}
