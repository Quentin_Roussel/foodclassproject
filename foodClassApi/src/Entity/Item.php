<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
class Item
{
    use EntityBag;

    #[ORM\Column(length: 255)]
    #[Groups(['item:list', 'item:view'])]
    private ?string $label = null;

    #[ORM\ManyToOne(inversedBy: 'items')]
    #[Groups(['item:view_list'])]
    private ?ItemList $itemList = null;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: SharedItem::class, cascade: ['remove'])]
    #[Groups(['item:shared', 'item:view'])]
    private Collection $sharedItems;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['item:list', 'item:view'])]
    private ?string $targetLink = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2, nullable: true)]
    #[Groups(['item:list', 'item:view'])]
    private ?string $idealPrice = null;

    public function __construct()
    {
        $this->sharedItems = new ArrayCollection();
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getItemList(): ?ItemList
    {
        return $this->itemList;
    }

    public function setItemList(?ItemList $itemList): static
    {
        $this->itemList = $itemList;

        return $this;
    }

    /**
     * @return Collection<int, SharedItem>
     */
    public function getSharedItems(): Collection
    {
        return $this->sharedItems;
    }

    public function addSharedItem(SharedItem $sharedItem): static
    {
        if (!$this->sharedItems->contains($sharedItem)) {
            $this->sharedItems->add($sharedItem);
            $sharedItem->setItem($this);
        }

        return $this;
    }

    public function removeSharedItem(SharedItem $sharedItem): static
    {
        if ($this->sharedItems->removeElement($sharedItem)) {
            // set the owning side to null (unless already changed)
            if ($sharedItem->getItem() === $this) {
                $sharedItem->setItem(null);
            }
        }

        return $this;
    }

    public function getTargetLink(): ?string
    {
        return $this->targetLink;
    }

    public function setTargetLink(?string $targetLink): static
    {
        $this->targetLink = $targetLink;

        return $this;
    }

    public function getIdealPrice(): ?string
    {
        return $this->idealPrice;
    }

    public function setIdealPrice(?string $idealPrice): static
    {
        $this->idealPrice = $idealPrice;

        return $this;
    }
}
