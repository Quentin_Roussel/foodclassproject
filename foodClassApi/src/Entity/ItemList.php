<?php

namespace App\Entity;

use App\Repository\ItemListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ItemListRepository::class)]
class ItemList
{
    use EntityBag;

    #[ORM\Column(length: 255)]
    #[Groups(['item:list', 'item:view', 'item:view_list'])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'itemList', targetEntity: Item::class, cascade: ['persist', 'remove'])]
    #[Groups(['item:view', 'item:shared'])]
    private Collection $items;

    #[ORM\ManyToOne(inversedBy: 'itemLists')]
    #[Groups(['item:list', 'item:view', 'item:view_list'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author = null;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): static
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setItemList($this);
        }

        return $this;
    }

    public function removeItem(Item $item): static
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getItemList() === $this) {
                $item->setItemList(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;

        return $this;
    }
}
