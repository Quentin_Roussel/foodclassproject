<?php

namespace App\Entity;

use App\Repository\SharedItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SharedItemRepository::class)]
class SharedItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['item:list', 'item:view', 'item:shared'])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'sharedItems')]
    #[Groups(['item:list', 'item:view', 'item:shared'])]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'sharedItems')]
    #[Groups(['item:list'])]
    private ?Item $item = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): static
    {
        $this->item = $item;

        return $this;
    }
}
