<?php

namespace App\Repository;

use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Item>
 */
abstract class AbstractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    public function findAllByFlags(string $alias = 'a', array $flags = [])
    {
        $qb = $this->createQueryBuilder($alias);

        $this->applyTableFiltering($qb, $alias, $flags);
        $this->applyFlags($qb, $alias, $flags);

        return $qb->getQuery()->getResult();
    }

    public function countAllByFlags(string $alias = 'a', array $flags = []): int
    {
        $qb = $this->createQueryBuilder($alias)->select("count($alias.id)");

        $this->applyFlags($qb, $alias, $flags);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function applyTableFiltering(QueryBuilder $qb, string $alias, array $flags = []): QueryBuilder
    {
        $limit = 10;
        $direction = 'asc';
        $orderBy = null;
        $offset = 0;
        
        if (isset($flags['limit'])) {
            $limit = (int) $flags['limit'];
        }

        if (isset($flags['orderBy'])) {
            $orderBy = (string) $flags['orderBy'];    
        }

        if (isset($flags['direction'])) {
            $direction = (string) $flags['direction'];    
        }

        if (isset($flags['offset'])) {
            $offset = (int) $flags['offset'];
        }

        if (null !== $orderBy) {
            $qb->orderBy("$alias.$orderBy", $direction);
        }


        $qb->setFirstResult($offset * $limit);
        $qb->setMaxResults($limit);

        return $qb;
    }

    public function applyFlags(QueryBuilder $qb, string $alias, array $flags = []): QueryBuilder
    {
        $filterBySingleId = $flags['filterBySingleId'] ?? null;

        if (null !== $filterBySingleId) {
            $filterBySingleId = explode('|', $filterBySingleId);

            foreach ($filterBySingleId as $fBsId) {
                $e = explode(':', $fBsId);

                $x = $e[0];

                if (false === in_array($e[0], $qb->getAllAliases())) {
                    if (true === isset($this->getClassMetadata()->getAssociationMappings()[$e[0]])) {
                        $qb->leftJoin("$alias.{$e[0]}", $e[0]);
                        $x .= '.id';
                        $qb->andWhere($qb->expr()->eq(
                            $x, $qb->expr()->literal($e[1])
                        ));
                    }
                }
            }
        }

        return $qb;
    }
}
