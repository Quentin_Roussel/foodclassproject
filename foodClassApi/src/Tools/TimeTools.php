<?php

namespace App\Tools;

use DateTime;

class TimeTools
{
    public static function periodIsPassed(DateTime $start, DateTime $end): bool
    {
        $today = new DateTime();

        return $today > $start && $today > $end;
    }
}
