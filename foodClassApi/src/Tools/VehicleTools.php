<?php

namespace App\Tools;

class VehicleTools
{
    public static function parseRegistration(string $registration): string
    {
        $newRegistration = $registration;
        $final = '';
        $totalLetter = 8; // start to 0

        for ($i = 0; $i <= $totalLetter; $i++) {
            if ((strlen($newRegistration) - 1) < $i) {
                continue;
            }
            $char = $newRegistration[$i];

            if (true === in_array($i, [2, 6])) {
                if ('-' !== $char) {
                    $final .= '-';
                }
            }

            $final .= $char;
        }

        return $final;
    }    
}