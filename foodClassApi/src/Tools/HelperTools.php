<?php

namespace App\Tools;

use DateTime;
use Symfony\Component\HttpFoundation\Request;

class HelperTools
{
    const THIS_CHART_CAN_BE_TO_WORLD = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿ';

    const INTERVAL_START_END_DAY = 5;
    const INTERVAL_START_END_WEEK = 1;
    const INTERVAL_START_END_MONTH = 2;
    const INTERVAL_START_END_TRIMESTER = 3;
    const INTERVAL_START_END_YEAR = 4;

    const ABBR_MONTHS = ['', 'Janv', 'Févr', 'Mars', 'Avril', 'Mai', 'Juin', 'Jui', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'];

    /**
     * @param string the text to slugify
     * @return string the text to slug format
     */
    public static function slugify(string $text): string
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function priceAmount(float $amount, int $decimals = 2, string $decimalSeparator = ',', string $thousandSeparator = ' '): string
    {
        return number_format($amount, $decimals, $decimalSeparator, $thousandSeparator);
    }

    public static function isLocalDev(): bool
    {
        return isset($_SERVER['APP_ENV']) && ('dev' === $_SERVER['APP_ENV']);
    }

    public static function shortString(?string $string, string $suffix = '...', int $max = 15): ?string
    {
        if (null === $string) {
            return null;
        }

        $nbWorlds = str_word_count($string, 0, self::THIS_CHART_CAN_BE_TO_WORLD);

        if ($max >= $nbWorlds) {
            return $string;
        }

        $worlds = str_word_count($string, 1, self::THIS_CHART_CAN_BE_TO_WORLD);

        $final = '';

        for ($i = 0; $i <= $max; $i++) {
            $final .= $worlds[$i] . (($max === $i) ? '' : ' ');
        }

        return $final . $suffix;
    }

    /**
     * @param string $formattedPhoneNumber
     * @return string
     */
    public static function cleanPhoneNumber(string $formattedPhoneNumber)
    {
        $formattedPhoneNumber = str_replace('-', '', $formattedPhoneNumber);
        $formattedPhoneNumber = str_replace('.', '', $formattedPhoneNumber);
        $formattedPhoneNumber = str_replace('/', '', $formattedPhoneNumber);
        $formattedPhoneNumber = str_replace(' ', '', $formattedPhoneNumber);
        $formattedPhoneNumber = str_replace('+', '', $formattedPhoneNumber);
    
        return $formattedPhoneNumber;
    }

    public static function combineRequestGroups(Request $request, array $baseGroups = []): array
    {
        $rGroups = $request->query->get('groups');

        if (null !== $rGroups) {
            if (preg_match('/;/', $rGroups)) {
                $rGroups = explode(';', $rGroups);
                $baseGroups = array_merge($baseGroups, $rGroups);
            } else {
                $baseGroups[] = $rGroups;
            }
        }

        return $baseGroups;
    }

    public static function updatedObjectToJson(object $object)
    {
        $fields = [];


        foreach ($object as $key => $value) {
            if (!in_array($key, ['employees', 'mainEmployees'])) {
                
                // dd($key);
            }
            $fields[$key] = $value;
        }

        return json_encode($fields);
    }

    /**
     * @param int $type
     * @param DateTime $startAt
     * @param bool $returnDateTimeObject
     */
    public static function generateInterval(int $type, DateTime $startAt, bool $returnDateTimeObject = true): array
    {
        $final = [
            'startAt' => null,
            'endAt' => null
        ];

        $year = $startAt->format('Y');
        $month = $startAt->format('m');
        $week = $startAt->format('W');

        $endAt = clone $startAt;

        switch ($type) {
            case self::INTERVAL_START_END_DAY:
                $startAt = new DateTime();
                $startAt->setISODate((int) $year, (int) $week);
                $endAt = clone $startAt;
            break;
            case self::INTERVAL_START_END_WEEK:
                $startAt = new DateTime();
                $startAt->setISODate((int) $year, (int) $week);
                $endAt = clone $startAt;
                $endAt->modify('+6 days');
            break;
            case self::INTERVAL_START_END_MONTH:
                $startAt = new DateTime("$year-$month-01");
                $endAt = new DateTime("$year-$month-" . date('t', strtotime($startAt->format('Y-m-d'))));
            break;
            case self::INTERVAL_START_END_TRIMESTER:
                $first = [1, 2, 3];
                $second = [4, 5, 6];
                $three = [7, 8, 9];
                if (true === in_array((int) $month, $first)) {
                    $startAt = new DateTime("$year-01-01");
                    $endAt = new DateTime("$year-03-31");
                } else if (true === in_array((int) $month, $second)) {
                    $startAt = new DateTime("$year-04-01");
                    $endAt = new DateTime("$year-06-30");
                } else if (true === in_array((int) $month, $three)) {
                    $startAt = new DateTime("$year-07-01");
                    $endAt = new DateTime("$year-09-30");
                } else {
                    $startAt = new DateTime("$year-10-01");
                    $endAt = new DateTime("$year-12-31");
                }
            break;
            case self::INTERVAL_START_END_YEAR:
                $startAt = new DateTime("$year-01-01");
                $endAt = new DateTime("$year-12-31");
            break;
        }

        $final['startAt'] = new DateTime($startAt->format('Y-m-d 00:00:00'));
        $final['endAt'] = new DateTime($endAt->format('Y-m-d 23:59:59'));

        if (false === $returnDateTimeObject) {
            $final['startAt'] = null === $startAt ? null : $startAt->format('Y-m-d 00:00:00');
            $final['endAt'] = null === $endAt ? null : $endAt->format('Y-m-d 23:59:59');
        }

        return $final;
    }

    public static function getAbbrMonth(int $monthNumber = 1): string
    {
        return self::ABBR_MONTHS[$monthNumber];
    }
}