<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController
{
    #[Route('/', name: 'app_index')]
    public function index(UserPasswordHasherInterface $ph, EntityManagerInterface $em): Response
    {
        $p = new User();
        $hp = $ph->hashPassword($p, 'password');
        $p->setPassword($hp);
        $p->setEmail('eee@eee.fr');
        $em->persist($p);
        $em->flush();

        return new Response('/');
    }
}