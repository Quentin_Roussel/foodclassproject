<?php

namespace App\Controller\Api;

use App\Form\Serializer\FormErrorSerializer;
use App\Service\LoggerService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class AbstractApiController extends AbstractFOSRestController
{
    protected $em;
    protected $kernelInterface;
    protected $errors = [];
    protected $formErrorSerializer;
    /** @var ApiUser|null $apiUser */
    protected $apiUser;

    public function __construct(EntityManagerInterface $em, KernelInterface $kernelInterface, 
    FormErrorSerializer $formErrorSerializer)
    {
        $this->em = $em;
        $this->kernelInterface = $kernelInterface;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    public function getVersion()
    {
        return $this->kernelInterface->getContainer()->getParameter('version');
    }

    public function isRequestFromCustomerArea()
    {
        if (null === $this->apiUser) {
            return false;
        }

        return null !== $this->apiUser->getInterlocutor();
    }

    protected function getErrors(): ?string
    {
        if (empty($this->errors)) {
            return null;
        }

        $errors = implode('<br />', $this->errors);

        return $errors;
    }

    protected function addError($key, string $error): self
    {
        $this->errors[$key] = $error;

        return $this;
    }

    protected function getCurrentApiUser()
    {
        return $this->getUser();
    }

    protected function countErrors(): int
    {
        return count($this->errors);
    }
}