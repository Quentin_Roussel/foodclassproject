<?php

namespace App\Controller\Api;

use App\Controller\Api\AbstractApiController;
use App\Entity\Item;
use App\Entity\ItemList;
use App\Entity\SharedItem;
use App\Entity\User;
use App\Form\Api\ItemListType;
use App\Service\ItemService;
use App\Tools\HelperTools;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/api/{entityName}/', name: 'app_api_itemList_')]
class ItemListController extends AbstractApiController
{
    #[View()]
    #[Rest\Get('items', name: 'list')]
    public function getItemsAction(ItemService $service, Request $request, string $entityName)
    {
        $flags = $request->query->all();

        return $this->json([
            'items' => $service->findAllByFlags($entityName, $flags),
            'total_items' => $service->countAllByFlags($entityName, $flags)
        ], Response::HTTP_OK, [], [
            'groups' => HelperTools::combineRequestGroups($request, ['item:list'])
        ]);
    }

    #[View()]
    #[Rest\Get('item/{query}/{value}', name: 'get')]
    public function getItemByQueryAction(ItemService $service, Request $request, string $entityName,
    string $query, string $value)
    {
        $item = $service->findByQuery($entityName, $query, $value);

        if ('itemList' === $entityName) {
            /** @var User $user */
            $user = $this->getUser();
            if ($user->getId() !== $item->getAuthor()->getId()) {
                return $this->json(['message' => 'Access denied.'], Response::HTTP_UNAUTHORIZED);
            }   
        }

        return $this->json($item,
        null === $item ? Response::HTTP_NOT_FOUND : Response::HTTP_OK, [], [
            'groups' => HelperTools::combineRequestGroups($request, ['item:view'])
        ]);
    }

    #[View()]
    #[Rest\Post('item/edit/{item}', name: 'edit', requirements: ['item' => '\d+'])]
    public function postItemListAction(Request $request, ItemService $itemService, ?int $item = null): JsonResponse
    {
        $data = $request->request->all();

        if (null !== $item) {
            $item = $this->em->getRepository(ItemList::class)->findOneBy(['id' => $item]);
        }

        $form = $this->createForm(ItemListType::class, $item);

        $form->submit($data, false);
        if (false === $form->isValid()) {
            $errors = $this->formErrorSerializer->arrayToString($form, $data);

            return $this->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }
        
        $item = $form->getData();

        $item->setAuthor($this->getUser());
        $itemService->edit($item);

        return $this->json($item, Response::HTTP_OK, [], [
            'groups' => 'item:list'
        ]);
    }

    #[View()]
    #[Rest\Post('item/to-share', name: 'to_share')]
    public function postItemToShareAction(Request $request, ItemService $itemService): JsonResponse
    {
        $data = $request->request->all();

        $state = $itemService->addSharedItemToUser($data['user'], $data['itemToShare']);

        return $this->json([
            'state' => $state
        ]);
    }

    #[View()]
    #[Rest\Post('item/to/{what}', name: 'to_custom')]
    public function postItemToCustomAction(Request $request, ItemService $itemService, string $what): JsonResponse
    {
        $allowedWhats = ['delete', 'unshare'];

        if (false === in_array($what, $allowedWhats)) {
            return $this->json(['error' => "Parameter $what is not allowed."], Response::HTTP_BAD_REQUEST);
        }

        $data = $request->request->all();

        switch ($what) {
            case 'delete':
                $state = $itemService->deleteSharedItems($data['itemIds']);
            break;
            case 'unshare':
                $state = $itemService->unsharedItemToUser($data['user'], $data['itemIds']);
            break;
        }

        return $this->json([
            'state' => $state
        ]);
    }

    #[View()]
    #[Rest\Delete('item/{id}', name: 'delete')]
    public function deleteItemByQueryAction(ItemService $service, Request $request, string $entityName,
    string $id)
    {
        $item = $service->getRepository($entityName)->findOneBy(['id' => $id]);

        if (null !== $item) {
            switch ($entityName) {
                case 'itemList':
                    $service->deleteItemList($item);
                break;
                case 'item':
                    $service->deleteItem($item);
                break;
            }
        }

        return $this->json([
            'state' => true
        ]);
    }
}
