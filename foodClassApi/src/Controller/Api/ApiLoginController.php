<?php

namespace App\Controller\Api;

use App\Controller\Api\AbstractApiController;
use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/api', name: 'app_api_')]
class ApiLoginController extends AbstractApiController
{
    #[View()]
    #[Rest\Post('/login', name: 'login')]
    public function index(UserService $userService, #[CurrentUser] ?User $user): JsonResponse
    {
        if (null === $user) {
            return $this->json([
                'message' => 'cannot retrieve user object',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $token = sha1(uniqid(time()));
        $user->setApiToken($token);
        $userService->edit($user);

        return $this->json([
            'email'  => $user->getUserIdentifier(),
            'id' => $user->getId(),
            'token' => $token,
        ]);
    }

    #[View()]
    #[Rest\Post('/sign-in', name: 'sign_in')]
    public function signIn(Request $request, UserService $userService): JsonResponse
    {
        $email = $request->get('username');

        if (null !== $email) {
            if (true === $hasSignIn = $userService->signIn($email)) {
                return $this->json(['state' => true]);
            }

            return $this->json([
                'state' => false,
                'error' => $userService->currentErrorString
            ]);
        }

        return $this->json([
            'state' => false,
            'error' => 'Pas de mail'
        ]);
    }

    #[View()]
    #[Rest\Post('/sign-in/password', name: 'sign_in_password')]
    public function signInPassword(Request $request, UserService $userService, UserPasswordHasherInterface $ph): JsonResponse
    {
        $token = $request->get('token');

        if (false === $userService->validateToken($token)) {
            return $this->json([
                'state' => false,
                'error' => 'Token de demande invalide'
            ]);
        }

        $newPassword = $request->get('newPassword');
        /** @var User $user */
        $user = $userService->getRepository('user')->findOneBy(['signInToken' => $token]);
        $user->setPassword($ph->hashPassword($user, $newPassword));
        $user->setSignInToken(null);
        $user->setSignInExpireAt(null);
        $userService->edit($user);

        return $this->json([
            'state' => true
        ]);
    }

    #[View()]
    #[Rest\Get('/sign-in/token/{token}', name: 'sign_in_token')]
    public function singInToken(UserService $userService, string $token): JsonResponse
    {
        return $this->json([
            'state' => $userService->validateToken($token)
        ]);
    }
}
