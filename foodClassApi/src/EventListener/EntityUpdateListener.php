<?php

namespace App\EventListener;

use App\Entity\EntityUpdateHistory;
use DateTime;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;

#[AsDoctrineListener(event: Events::postPersist, priority: 500, connection: 'default')]
#[AsDoctrineListener(event: Events::postUpdate, priority: 500, connection: 'default')]
#[AsDoctrineListener(event: Events::preUpdate, priority: 500, connection: 'default')]
class EntityUpdateListener extends AbstractListener
{
    // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(PostPersistEventArgs $args): void
    {
        $entity = $args->getObject();

        // ... do something to notify the changes
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        // $disabledHistory = ['entityUpdateHistory'];
        // $allowedHistory = ['interlocutor', 'user', 'customer', 'address', 'customentityvalue'];
        // $unallowedHistoryFields = ['createdAt', 'updatedAt'];
        // $className = get_class($args->getObject());
        // $entityIdentifier = strtolower(str_replace('App\\Entity\\', '', $className));
        // if (true === in_array($entityIdentifier, $disabledHistory)) { 
        //     return;
        // }
        // if (false === in_array($entityIdentifier, $allowedHistory)) { 
        //     return;
        // }

        // $s = [];

        // foreach ($args->getEntityChangeSet() as $key => $changeSet) {
        //     if (true === in_array($key, $unallowedHistoryFields)) { continue; }
        //     $s[$key] = [
        //         'old' => $changeSet[0],
        //         'new' => $changeSet[1]
        //     ];
        // }

        // if (empty($s)) { return; }

        // $n = new EntityUpdateHistory();
        // $n->setChanges(json_encode($s));
        // $n->setEntityIdentifier($entityIdentifier);
        // $n->setEntityValue($args->getObject()->getId());
        // $n->setUpdatedAt(new DateTime());
        // $n->setCreatedAt(new DateTimeImmutable());
        // $n->setIsActive(1);

        // $this->em->persist($n);
    }

    public function postUpdate(PostUpdateEventArgs $args): void
    {
        // $entity = $args->getObject();

        // $n = new EntityUpdateHistory();

        // $this->em->flush();
        // ... do something to notify the changes
    }
}