<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class AbstractListener
{
    public $em;

    public function __construct(EntityManagerInterface $em, KernelInterface $kernelInterface)
    {
        $this->em = $em;    
    }
}