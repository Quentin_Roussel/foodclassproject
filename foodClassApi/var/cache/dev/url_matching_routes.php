<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/api/login' => [[['_route' => 'app_api_login', '_controller' => 'App\\Controller\\Api\\ApiLoginController::index'], null, ['POST' => 0], null, false, false, null]],
        '/api/sign-in' => [[['_route' => 'app_api_sign_in', '_controller' => 'App\\Controller\\Api\\ApiLoginController::signIn'], null, ['POST' => 0], null, false, false, null]],
        '/api/sign-in/password' => [[['_route' => 'app_api_sign_in_password', '_controller' => 'App\\Controller\\Api\\ApiLoginController::signInPassword'], null, ['POST' => 0], null, false, false, null]],
        '/' => [[['_route' => 'app_index', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/api/(?'
                    .'|sign\\-in/token/([^/]++)(*:38)'
                    .'|([^/]++)/item(?'
                        .'|s(*:62)'
                        .'|/(?'
                            .'|([^/]++)/([^/]++)(*:90)'
                            .'|edit(?:/(\\d+))?(*:112)'
                            .'|to(?'
                                .'|\\-share(*:132)'
                                .'|/([^/]++)(*:149)'
                            .')'
                            .'|([^/]++)(*:166)'
                        .')'
                    .')'
                .')'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:208)'
                    .'|wdt/([^/]++)(*:228)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:274)'
                            .'|router(*:288)'
                            .'|exception(?'
                                .'|(*:308)'
                                .'|\\.css(*:321)'
                            .')'
                        .')'
                        .'|(*:331)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => 'app_api_sign_in_token', '_controller' => 'App\\Controller\\Api\\ApiLoginController::singInToken'], ['token'], ['GET' => 0], null, false, true, null]],
        62 => [[['_route' => 'app_api_itemList_list', '_controller' => 'App\\Controller\\Api\\ItemListController::getItemsAction'], ['entityName'], ['GET' => 0], null, false, false, null]],
        90 => [[['_route' => 'app_api_itemList_get', '_controller' => 'App\\Controller\\Api\\ItemListController::getItemByQueryAction'], ['entityName', 'query', 'value'], ['GET' => 0], null, false, true, null]],
        112 => [[['_route' => 'app_api_itemList_edit', 'item' => null, '_controller' => 'App\\Controller\\Api\\ItemListController::postItemListAction'], ['entityName', 'item'], ['POST' => 0], null, false, true, null]],
        132 => [[['_route' => 'app_api_itemList_to_share', '_controller' => 'App\\Controller\\Api\\ItemListController::postItemToShareAction'], ['entityName'], ['POST' => 0], null, false, false, null]],
        149 => [[['_route' => 'app_api_itemList_to_custom', '_controller' => 'App\\Controller\\Api\\ItemListController::postItemToCustomAction'], ['entityName', 'what'], ['POST' => 0], null, false, true, null]],
        166 => [[['_route' => 'app_api_itemList_delete', '_controller' => 'App\\Controller\\Api\\ItemListController::deleteItemByQueryAction'], ['entityName', 'id'], ['DELETE' => 0], null, false, true, null]],
        208 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        228 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        274 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        288 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        308 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        321 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        331 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
