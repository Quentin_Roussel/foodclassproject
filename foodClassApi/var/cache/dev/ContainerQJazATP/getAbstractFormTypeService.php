<?php

namespace ContainerQJazATP;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getAbstractFormTypeService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Form\Api\AbstractFormType' shared autowired service.
     *
     * @return \App\Form\Api\AbstractFormType
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
        include_once \dirname(__DIR__, 4).'/src/Form/Api/AbstractFormType.php';

        return $container->privates['App\\Form\\Api\\AbstractFormType'] = new \App\Form\Api\AbstractFormType();
    }
}
