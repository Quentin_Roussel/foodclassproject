# FoodClass API

## Installation

1 - Créer un nouveau schéma dans votre base de données dédié qui sera dédié au projet.

2 - Créer un nouveau fichier .env.local en copiant/collant le contenu du fichier .env à la racine du répertoire foodClassApi/.

3 - Remplacer ensuite les variables de connexion à votre base de données (YOUR_USER, YOUR_PWD, YOUR_DB_NAME).

4 - Depuis le répertoire foodClassApi/ lancer la commande : **composer install**.

5 - Depuis le répertoire foodClassApi/ lancer la commande : **bin/console doctrine:migrations:migrate**.

6 - Depuis le répertoire foodClassApi/ lancer la commande : **symfony serve**.